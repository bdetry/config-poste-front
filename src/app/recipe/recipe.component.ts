import { Component, OnInit } from '@angular/core';
import { RecipService } from '../recip.service';
import { Recipe } from '../models/Recipe';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent implements OnInit {

  public recipe : Recipe
  public PdfUrl : string
  

  constructor(private recipeService : RecipService ,
              private route: ActivatedRoute,
              private sanitizer:DomSanitizer) {

                this.route.params.subscribe(p=>{
                  let id = p['id'];
                  this.PdfUrl = "https://recettes.baba.click/export-recette/"+id;

                  this.recipeService.getRecipe(id).then(recipe=>{
                    this.recipe = recipe
                  }).catch(err => console.log(err))
                })
    
   }

  ngOnInit() {
  }

  printPDF(){
    window.open(this.PdfUrl, '_blank');
  }

}
