export interface Recipe{
    id : number;
    titre : string;
    duree : number;
    description : string;
}