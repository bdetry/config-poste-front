import { Component, OnInit } from '@angular/core';
import { RecipService } from '../recip.service';
import { Recipe } from '../models/Recipe';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public recipes : Recipe[];

  constructor(private recipService : RecipService) {
    this.recipService.getRecipes().then(re=>{
      this.recipes = re
    }).catch(err => {
      console.log(err)
    })
   }

  ngOnInit() {
  }

}
