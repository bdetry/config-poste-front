import { Injectable } from '@angular/core';
import { Recipe } from './models/Recipe';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class RecipService {

  constructor(private http : HttpClient) { }

  public getRecipes() : Promise<Recipe[]>{
    return new Promise((resolve , reject)=>{
      this.http.get("https://recettes.baba.click/cool-recettes").subscribe(x=>{ 
        resolve(x as Recipe[])
      } , err =>{
        reject(err)
      })    
    })
  }

  public getRecipe(id : number) : Promise<Recipe>{
    return new Promise((resolve , reject)=>{
      this.http.get("https://recettes.baba.click/recettes/"+id).subscribe(x=>{ 
        resolve(x as Recipe)
      } , err =>{
        reject(err)
      })    
    })
  }
}
